<?php

/**
 * @file
 * NAT-ng module administrative forms.
 */

/**
 * Menu callback: NAT-ng module settings form.
 */
function nat_ng_settings_form() {
  $types = node_type_get_types();
  $vocabularies = _nat_ng_get_vocabularies();

  if (empty($vocabularies)) {
    drupal_set_message(t('The NAT-ng module requires at least one vocabulary to be defined.'), 'error');
    drupal_goto('admin/content/taxonomy');
  }

  $nat_ng_config = _nat_ng_variable_get();

  foreach ($types as $type => $type_object) {
    $collapsed = (!isset($nat_ng_config['types'][$type])) || (empty($nat_ng_config['types'][$type]));
    $form['nat_ng_' . $type] = array(
      '#type' => 'fieldset',
      '#title' => check_plain($type_object->name),
      '#collapsible' => TRUE,
      '#collapsed' => $collapsed
    );
    $form['nat_ng_' . $type][$type] = array(
      '#type' => 'select',
      '#title' => t('Vocabularies'),
      '#options' => $vocabularies,
      '#default_value' => isset($nat_ng_config['types'][$type]) ? $nat_ng_config['types'][$type] : array(),
      '#multiple' => TRUE,
      '#description' => t('Creating a node of type %type will automatically create a term in any selected vocabularies.', array('%type' => $type)),
      '#parents' => array('types', $type)
    );
    $form['nat_ng_' . $type]['body_' . $type] = array(
      '#type' => 'checkbox',
      '#title' => t('Associate node body with term description.'),
      '#default_value' => isset($nat_ng_config['body'][$type]) ? $nat_ng_config['body'][$type] : 0,
      '#parents' => array('body', $type)
    );
    $form['nat_ng_' . $type]['delete_' . $type] = array(
      '#type' => 'checkbox',
      '#title' => t('Delete associated term if a node is deleted.'),
      '#default_value' => isset($nat_ng_config['delete'][$type]) ? $nat_ng_config['delete'][$type] : 0,
      '#parents' => array('delete', $type)
    );
  }
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));

  return $form;
}

/**
 * Process NAT-ng settings form submissions.
 */
function nat_ng_settings_form_submit($form, &$form_state) {
  $form_values = $form_state['values'];

  unset($form_values['form_id'], $form_values['form_build_id'], $form_values['submit'], $form_values['op'], $form_values['form_token']);

  $form_values['body'] = array_filter($form_values['body']);
  $form_values['delete'] = array_filter($form_values['delete']);

  variable_set('nat_ng_config', $form_values);

  drupal_set_message(t('Configuration settings saved.'));
}

/**
 * Sync the NAT-ng table with the node table for associated vocabularies.
 */
function nat_ng_sync_form() {
  $vocabularies = _nat_ng_get_vocabularies();

  if (empty($vocabularies)) {
    drupal_set_message(t('The NAT-ng module requires at least one vocabulary to be defined.'), 'error');
    drupal_goto('admin/content/taxonomy');
  }

  $nat_ng_config = _nat_ng_variable_get();
  $options = array();
  foreach ($nat_ng_config['types'] as $type => $associations) {
    if (!empty($associations)) {
      foreach ($associations as $vid) {
        $options[$type . '|' . $vid] = t('@type &lsaquo;-&rsaquo; !vocabulary', array('@type' => $type, '!vocabulary' => $vocabularies[$vid]));
      }
    }
  }
  if (empty($options)) {
    drupal_set_message(t('There are no vocabularies available to sync.'));
    drupal_goto('admin/settings/nat-ng');
  }

  $form['sync'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sync associations'),
    '#description' => t('The Sync operation will create NAT-ng associations (and terms) for nodes (marked for NAT-ng association) not present in the NAT-ng table.'),
    '#collapsible' => TRUE
  );
  $form['sync']['vocabularies'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select the vocabularies to sync with associated node tables'),
    '#description' => t('Any nodes not already NAT-ng associated with the selected vocabularies will be associated.'),
    '#required' => TRUE,
    '#options' => $options
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Sync tables'));

  return $form;
}

/**
 * Process NAT-ng sync form submissions.
 */
function nat_ng_sync_form_submit($form, &$form_state) {
  _nat_ng_sync_associations(array_filter($form_state['values']['vocabularies']));
}

/**
 * Synchronize NAT-ng node-term relationships. Create associated terms for node
 * where missing.
 *
 * @param $associations
 *   Associative array denoting the node-vocabulary pair that is to be synced.
 */
function _nat_ng_sync_associations($associations) {
  $nat_ng_config = _nat_ng_variable_get();

  $counter = 0;
  foreach ($associations as $association) {
    $association = explode('|', $association);
    // This query can possibly be improved.
    $result = db_query("SELECT n.nid, n.type, n.title, nr.body FROM {node} n INNER JOIN {node_revisions} nr USING (vid) LEFT JOIN {nat_ng} n1 ON (n.nid = n1.nid AND n1.vid = %d) LEFT JOIN {nat_ng} n2 ON (n.nid = n2.nid AND n2.nid <> n1.nid) WHERE n.type = '%s' AND n1.nid IS NULL", $association[1], $association[0]);
    while ($node = db_fetch_object($result)) {
      // Add node title as terms.
      $terms = _nat_ng_add_terms($node, array($association[1]));

      // Save node-term association in the NAT-ng table.
      _nat_ng_save_association($node->nid, $terms);

      $counter++;
    }
  }
  drupal_set_message(t('NAT-ng sync complete: %count nodes synced.', array('%count' => $counter)));
}
