
NAT-ng is fork of NAT module, a helper module used to maintain node-term
relationships, i.e. when a node is created a taxonomy term is created
automatically using its title and body in any associated vocabularies. This
module also attempts to preserve hierarchical relationships where possible.

Project URL: http://drupal.org/project/nat_ng

Installation
------------

1. Unpack the nat_ng folder and contents in the appropriate modules directory of
   your Drupal installation. This is probably sites/all/modules/
2. Enable the NAT-ng module in the administration tools.
3. If you're not using Drupal's default administrative account, make
   sure "Administer NAT-ng configuration" is enabled through access control
   administration.
4. Visit the NAT-ng settings page and make appropriate configurations
   For 6.x: Administer > Site configuration > NAT-ng
   For 7.x: Administer > Structure > NAT-ng