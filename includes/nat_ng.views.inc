<?php

/*
 * @file
 * NAT-ng - Node Auto Term NG - Views module implementation.
 */

/**
 * Implements hook_views_handlers().
 */
function nat_ng_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'nat_ng') . '/includes',
    ),
    'handlers' => array(
      'nat_ng_handler_argument_term_node_tid_depth' => array(
        'parent' => 'views_handler_argument',
      ),
      'nat_ng_handler_argument_term_node_tid_depth_modifier' => array(
        'parent' => 'views_handler_argument',
      ),
    ),
  );
}

/*
 * Implements hook_views_data().
 */
function nat_ng_views_data() {
  $data = array();

  $data['nat_ng'] = array(
    'table' => array(
      'group' => t('NAT-ng'),
    ),
  );

  $data['nat_ng']['table']['join']['node'] = array(
    'left_table' => 'nat_ng_term_node',
    'left_field' => 'tid',
    'field' => 'tid',
  );

  $data['nat_ng_term_node']['table']['join']['node'] = array(
    'table' => 'taxonomy_index',
    'left_field' => 'nid',
    'field' => 'nid',
  );

  // add nat node to term view
  $data['nat_ng']['table']['join']['term_data'] = array(
    'left_field' => 'tid',
    'field' => 'tid',
  );

  $data['nat_ng']['nid'] = array(
    'title' => t('Nid'),
    'help' => t('The node ID of the NAT-ng node.'),
    'field' => array(
      'handler' => 'views_handler_field_node',
      'click sortable' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_node_nid',
      'name field' => 'title',
      'numeric' => TRUE,
      'validate type' => 'nid',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'label' => t('NAT-ng Node'),
    ),
  );

  // Term to NAT node handling
  $data['nat_ng_tid'] = array(
    'table' => array(
      'group' => t('NAT-ng'),
    ),
  );

  $data['nat_ng_tid']['table']['join']['node'] = array(
    'table' => 'nat_ng',
    'left_field' => 'nid',
    'field' => 'nid',
  );

  $data['nat_ng_tid']['tid'] = array(
    'title' => t('Tid'),
    'help' => t('The tid of the NAT-ng term.'),
    'argument' => array(
      'handler' => 'views_handler_argument_term_node_tid',
      'name field' => 'title',
      'numeric' => TRUE,
      'validate type' => 'tid',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  // Uses data from Term Node Count module
  if (module_exists('term_node_count')) {
    $data['nat_ng_term_node_count']['table']['group'] = t('NAT-ng');
    $data['nat_ng_term_node_count']['table']['join']['node'] = array(
      'table' => 'term_node_count',
      'left_table' => 'nat_ng_tnc',
      'left_field' => 'tid',
      'field' => 'tid',
    );
    $data['nat_ng_tnc']['table']['join']['node'] = array(
      'table' => 'nat_ng',
      'left_field' => 'nid',
      'field' => 'nid',
    );
    $data['nat_ng_term_node_count']['node_count'] = array(
      'field' => 'node_count',
      'title' => t('Node Count'),
      'help' => t('The number of nodes associated with NAT-ng term'),
      'filter' => array(
        'handler' => 'views_handler_filter_numeric',
      ),
    );
  }

  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function nat_ng_views_data_alter(&$data) {
  $data['node']['nat_ng_term_node_tid_depth'] = array(
    'group' => t('NAT-ng'),
    'title' => t('Nid (with depth)'),
    'help' => t('The node ID of the NAT-ng with depth.'),
    'real field' => 'vid',
    'argument' => array(
      'handler' => 'nat_ng_handler_argument_term_node_tid_depth',
      'accept depth modifier' => TRUE,
    ),
  );

  $data['node']['nat_ng_term_node_tid_depth_modifier'] = array(
    'group' => t('NAT-ng'),
    'title' => t('Nid depth modifier'),
    'help' => t('Allows the "depth" for NAT-ng: Nid (with depth) to be modified via an additional argument.'),
    'argument' => array(
      'handler' => 'nat_ng_handler_argument_term_node_tid_depth_modifier',
    ),
  );
}
